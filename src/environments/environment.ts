// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBVxtNb8MxxfsqaCjCwonP3pWUzEQhL3U4',
    authDomain: 'fir-test-1-97fe9.firebaseapp.com',
    databaseURL: 'https://fir-test-1-97fe9.firebaseio.com',
    projectId: 'fir-test-1-97fe9',
    storageBucket: 'fir-test-1-97fe9.appspot.com',
    messagingSenderId: '443538824603'
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
