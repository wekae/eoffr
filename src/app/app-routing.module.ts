import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Routes, RouterModule} from '@angular/router';


import {HomeComponent} from './components/home/home.component';
import {OfferDetailsComponent} from './components/offer-details/offer-details.component';
import {OffersComponent} from './components/offers/offers.component';

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'offers', component: OffersComponent},
  {path: 'details/:itemId', component: OfferDetailsComponent},
]


@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
