import {Offer} from './offer';

export class Group {

  categoryImageUrl: string;
  groupname: string;
  members: Offer[];

}
