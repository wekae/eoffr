export class Offer {

  active: boolean;
  categories: string[];
  description: string;
  enddate: string;
  featured: false;
  id: number;
  imageUrl: string;
  outletname: string;
  startdate: string;

}
