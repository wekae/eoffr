export class Item {

  description: string;
  finalCost: number;
  initialCost: number;
  itemid: number;
  name: string;
  offer: string;
  offerType: string;
  thumbnailUrl: string;

}
